#!/usr/bin/python3
import os
import re
import pickle
import sys

from UserTime import User, Time

# Provide path to log directory
LOG_PATH = sys.argv[1]

# Global data structures initialization
if os.path.isfile("temp"):
    with open("temp", 'rb') as pickled:
        CURRENT_USERS = pickle.load(pickled)
else:
    CURRENT_USERS = {}

FOR_PRINT = []


def oldest(files):
    """Choosing oldest file from list of file names

    Input: list of files
    Output: oldest file name
    """

    oldest_one = min([item[4:6] + item[0:4]
                      for item in [file.replace('-', '').replace('.txt', '') for file in files]])

    return "{2}{3}-{4}{5}-{0}{1}.txt".format(*oldest_one)


def current_file(hist):
    """Looking for history file, if not found, loading all log files in specified directory
    and choosing oldest of them

    Input: path to history file
    Output: current file name
    """

    if os.path.isfile(hist):
        with open(hist, 'r') as history:
            current_string = history.read()
            if re.match(r'\d\d-\d\d-\d\d\.txt:\d*', current_string):
                current = current_string.split(':')
                return current

    log_name = re.compile(r'\d\d-\d\d-\d\d\.txt')
    log_files = [os.path.join(name)
                 for root, dirs, files in os.walk(LOG_PATH)
                 for name in files if re.match(log_name, name)]

    current = (oldest(log_files), 0)
    return current


# Regex patterns compilation

# connect = re.compile(r'\[\d\d:\d\d:\d\d\]\sClient\s\".*\"\sconnected\.')
SPAWN = re.compile(r'\[\d\d:\d\d:\d\d\]\sClient\s\".*\"\sspawned in server\s<.*>')
DISC = re.compile(r'\[\d\d:\d\d:\d\d\]\sDropped\s\".*\"\sfrom server\s*<.*>')
ENDLINE = re.compile(r'<Logging continued in.*\d\d-\d\d-\d\d\.txt\">')
TIME = re.compile(r'(?<=\[)\d\d:\d\d:\d\d(?=\])')
USER = re.compile(r'(?<=\").*(?=\")')
STEAM_ID = re.compile(r'(?<=<).*(?=>)')
NEXT_FILE = re.compile(r'\d\d-\d\d-\d\d\.txt')


def printme(user):
    """If user has spawned, he is added to to_be_printed list and removed
    from current_user dictionary
    """
    FOR_PRINT.append(repr(user))
    del CURRENT_USERS[user.steamid]


def spawning(line_time, nickname, line_steamid):
    """When current line matches spawn pattern, function is invoked.
    Adds user, when not in current_users and grants steamid.
    Checks if steamid matches for spawned users."""

    if line_steamid not in CURRENT_USERS:
        if line_steamid == "BOT":
            return
        else:
            user = User(line_steamid)
            user.contime = line_time
            user.nick = nickname
            user.last_activity = line_time
            CURRENT_USERS[line_steamid] = user
    else:
        CURRENT_USERS[line_steamid].last_activity = line_time


def disconnection(line_time, line_steamid):
    """When current line matches drop pattern, function is invoked.
    Checks if steamid matches, grants droptime and invokes printme()
    """

    if line_steamid in CURRENT_USERS:
        CURRENT_USERS[line_steamid].last_activity = line_time
        printme(CURRENT_USERS[line_steamid])


def take_next(line):
    """When current line matches endfile pattern, function is invoekd.
    Clears current_users dictionary, transfers users that can be printed
    to to_be_printed

    Output: Tuple (Name of next file, 0 --> current byte in file)"""

    for steamid in CURRENT_USERS.copy():
        printme(CURRENT_USERS[steamid])
    return re.findall(NEXT_FILE, line)[0], 0


def action(line):
    """Decides what action to take.
    Can invoke connection, spawning, disconnection, take_next if certain
    patterns are matched.
    Otherwise checks for users activity."""

    if re.match(SPAWN, line):
        line_time = Time(re.findall(TIME, line)[0])
        nickname = re.findall(USER, line)[0]
        steamid = re.findall(STEAM_ID, line)[0]
        spawning(line_time, nickname, steamid)

    elif re.match(DISC, line):
        line_time = Time(re.findall(TIME, line)[0])
        steamid = re.findall(STEAM_ID, line)[0]
        disconnection(line_time, steamid)

    elif re.match(ENDLINE, line):
        return take_next(line)

    else:
        for user in CURRENT_USERS.values():
            if user.nick in line:
                line_time = Time(re.findall(TIME, line)[0])
                CURRENT_USERS[user.steamid].last_activity = line_time


def missing_endline(filename):
    """Jak jebany Tomek nie umie sobie zrobic, zeby ostatnia linijka byla zawsze taka sama"""
    log_name = re.compile(r'\d\d-\d\d-\d\d\.txt')
    log_files = [os.path.join(name)
                 for root, dirs, files in os.walk(LOG_PATH)
                 for name in files if re.match(log_name, name)]
    oldest_file = oldest(log_files)
    while oldest_file != filename:
        log_files.remove(oldest_file)
        oldest_file = oldest(log_files)
    log_files.remove(oldest_file)
    if log_files:
        return oldest(log_files)
    else:
        return


def parse_file(path, end_byte):
    print(path)
    if int(end_byte) == 0:
        FOR_PRINT.append(path)

    with open(LOG_PATH + path, 'rb') as file:
        with open(".history", 'w') as history:
            file.seek(int(end_byte))
            for line in file:
                line = line.decode(encoding="utf-8", errors="replace")
                if action(line):
                    to_parse = action(line)
                    end_point = "%s:%s" % (to_parse[0], to_parse[1])
                    history.write(end_point)
                    return
            if missing_endline(path):
                take_next('<Logging continued in "data/ulx_logs/02-21-17.txt">')
                history.write("%s:0" % missing_endline(path))
                return
            else:
                end_byte = file.tell()
                end_point = "%s:%s" % (path, end_byte)
                history.write(end_point)
            return True


def main():
    hist_path = '.history'
    while True:
        finished = current_file(hist_path)
        if parse_file(finished[0], finished[1]):
            break

    with open("logHistory", 'a') as log_history:
        for line in FOR_PRINT:
            log_history.write("%s\n" % line)

    with open("temp", 'wb') as pickling:
        pickle.dump(CURRENT_USERS, pickling)

    print("Users added to file %s\nUsers in queue %s" % (len(FOR_PRINT), len(CURRENT_USERS)))
if __name__ == '__main__':
    main()
