class User:
    def __init__(self, steamid):
        self.nick = ''
        self.contime = Time(0)
        self.steamid = steamid
        self.last_activity = Time(0)

    def getTime(self):
        return self.last_activity - self.contime

    def __repr__(self):
        return "%s-%s-%s" % (self.nick, self.getTime().time, self.steamid)


class Time:
    def __init__(self, stringtime):
        multi = [3600, 60, 1]
        if type(stringtime) == str:
            self.time = sum([int(stringtime.split(':')[i]) * multi[i] for i in range(3)])
        else:
            self.time = stringtime

    def __repr__(self):
        hours = self.time // 3600
        minutes = (self.time % 3600) // 60
        seconds = (self.time % 3600) % 60
        return str("%02d:%02d:%02d" % (hours, minutes, seconds))

    def __sub__(self, other):
        diff = self.time - other.time
        if diff >= 0:
            return Time(diff)
        else:
            raise ValueError
